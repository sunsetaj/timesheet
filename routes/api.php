<?php

use Illuminate\Http\Request;
use App\User;
use App\Timesheet as Timesheet;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', function() {
    return User::all();
});

Route::get('/timesheets/{user}', function(Request $request, $user) {
    return Timesheet::where('user', $user)
                        ->orderBy('date', 'desc')
                        ->get();
});

Route::get('/timesheets/daily/{user}', function(Request $request, $user) {
    return Timesheet::where('user', $user)
                        ->whereDate('date', '>=', Carbon::yesterday())
                        ->orderBy('date', 'desc')
                        ->get();
});

Route::get('/timesheet/{id}', function(Request $request, $id) {
    return Timesheet::where('id', $id)->first();
});
