<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Timesheet routes
Route::get('/timesheet/daily', 'TimesheetController@daily')->name('timesheet.daily');
Route::post('/timesheet', 'TimesheetController@store');
Route::delete('/timesheet/delete/{id}', 'TimesheetController@destroy');
Route::put('timesheet/{id}', 'TimesheetController@update');

Route::resource('timesheet', 'TimesheetController');

// Project routes
Route::post('/project', 'ProjectController@store');

Route::resource('projects', 'ProjectController');