<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;

class Helper
{
    public static function menuState($route, $class = 'active')
    {
        if (Route::currentRouteNamed($route)) {
            return $class;
        } else {
            return false;
        }
    }

    /**
    * Helper function to strip tags on invalid characters
    * @param { Separator character } $separator, default ','
    * @param { Tag String } $tags, String
    * @return $tags
    */
    
    public static function cleanTags($separator = ',', $tags)
    {
        if ($tags == '') {
            return null;
        }
        else {
            $tags = explode($separator, $tags);
            for ($i = 0; $i < count($tags); $i++) {
                $tags[$i] = ltrim($tags[$i], ' ');
                $tags[$i] = rtrim($tags[$i], ' ');
                $tags[$i] = preg_replace('/[^a-z]/i', '', $tags[$i]);
            }
            $tags = implode($separator, $tags);
            $tags = ltrim($tags, $separator);
            $tags = rtrim($tags, $separator);
            
            return $tags;
        }
    }
}
