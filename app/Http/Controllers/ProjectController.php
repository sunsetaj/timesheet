<?php

namespace App\Http\Controllers;

use App\Project as Project;
use Illuminate\Http\Request;
use Illuminate\Htpp\Response as Response;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use App\Http\Requests\StoreProject as StoreProject;
use Validator;
use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\Helper as Helper;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Fetch Projects from database
        $projects = Project::where('user_id', Auth::user()->id)
                            ->orderBy('project_name', 'desc')
                            ->get();
        // Add projects information to global javascript variable
        JavaScript::put([
            'projects' => $projects,
            'user' => Auth::user()->id
        ]);
        // return view
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProject  $request
     * @return Response response
     */
    public function store(StoreProject $request)
    {
        $project = new Project;

        $project_name = filter_var($request->input('project_name'), FILTER_SANITIZE_STRING);

        $project->project_name = $project_name;
        $project->user_id = Auth::user()->id;

        $project->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project 
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
