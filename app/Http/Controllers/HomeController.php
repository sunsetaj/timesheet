<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use App\User as User;
use App\Timesheet as Timesheet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        JavaScript::put([
            'name' => Auth::user()->name,
            'id' => Auth::user()->id,
            'timesheets' => Timesheet::where('user', Auth::user()->id)
                                    ->orderBy('date', 'desc')
                                    ->get()
        ]);

        return view('home');
    }

    public function example($id)
    {
        $user = User::where('id', $id)->get();
        JavaScript::put([
            'user' => $user
        ]);
        return view('home');
    }
}
