<?php

namespace App\Http\Controllers;

use App\Timesheet as Timesheet;
use Illuminate\Http\Request;
use Illuminate\Http\Response as Response;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use App\Http\Requests\StoreTimesheet as StoreTimesheet;
use Validator;
use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\Helper as Helper;

class TimesheetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timesheets = Timesheet::where('user', Auth::user()->id)
                                ->orderBy('date', 'desc')
                                ->get();
        JavaScript::put([
            'timesheets' => $timesheets,
            'user' => Auth::user()->id
        ]);
        return view('home');
    }

    /**
    * Displays past few dailys of entries
    *
    * @return \Illuminate\Http\Response
    */
    public function daily()
    {
        $yesterday = Carbon::yesterday();

        $timesheets = Timesheet::where('user', Auth::user()->id)
                                ->whereDate('date', '>=', $yesterday)
                                ->orderBy('date', 'desc')
                                ->get();

        JavaScript::put([
            'timesheets' => $timesheets,
            'user' => Auth::user()->id
        ]);
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTimesheet  $request
     * @return Response response
     */
    public function store(StoreTimesheet $request)
    {
        $timesheet = new Timesheet;

        $task = filter_var($request->input('task'), FILTER_SANITIZE_STRING);
        $description = htmlspecialchars($request->input('description'));
        $time_spent = floatval($request->input('time_spent'));
        $tags = Helper::cleanTags(',', $request->input('tags'));
        $date = $request->input('date');

        $timesheet->user = Auth::user()->id;
        $timesheet->task = $task;
        $timesheet->description = $description;
        $timesheet->time_spent = $time_spent;
        $timesheet->tags = $tags;
        $timesheet->date = $date;

        $timesheet->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreTimesheet  $request
     * @return Response response
     */
    public function update(StoreTimesheet $request, $id)
    {
        $timesheet = Timesheet::find($id);

        $task = filter_var($request->input('task'), FILTER_SANITIZE_STRING);
        $description = htmlspecialchars($request->input('description'));
        $time_spent = floatval($request->input('time_spent'));
        $tags = Helper::cleanTags(',', $request->input('tags'));
        $date = $request->input('date');

        $timesheet->user = Auth::user()->id;
        $timesheet->task = $task;
        $timesheet->description = $description;
        $timesheet->time_spent = $time_spent;
        $timesheet->tags = $tags;
        $timesheet->date = $date;

        $timesheet->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timesheet  $timesheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timesheet $timesheet, $id)
    {
        $timesheet = Timesheet::find($id);
        $timesheet->delete();
    }

    /**
    *
    *   Get project associated with timesheet entry
    */
    public function project()
    {
        return $this->hasOne('App\Project');
    }
}
