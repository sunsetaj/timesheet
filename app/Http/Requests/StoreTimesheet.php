<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class StoreTimesheet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'integer',
            'task' => 'required|string',
            'description' => 'required|string|max:255',
            'time_spent' => 'required',
            'tags' => 'string|max:255|nullable',
            'date' => 'required|date'
        ];
    }
}
