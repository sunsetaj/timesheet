<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div id="navigation">
            <ul>
                @if (Auth::guest())
                    <li class="{{ Helper::menuState('login') }}"><a href="{{ route('login') }}">Login</a></li>
                    <li class="{{ Helper::menuState('register') }}"><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="{{ Helper::menuState('home') }}">
                        <a href="{{ route('home') }}">Dashboard</a>
                    </li>
                    <li class="{{ Helper::menuState('timesheet.index') }}">
                        <a href="{{ route('timesheet.index') }}">Timesheets</a>
                    </li>
                    <li class="{{ Helper::menuState('timesheet.daily') }}">
                        <a href="{{ route('timesheet.daily') }}">Daily Entries</a>
                    </li>
                    <li class="{{ Helper::menuState('projects.index') }}">
                        <a href="{{ route('projects.index') }}">Projects</a>
                    </li>
                    <li class="seperator"></li>
                    <li class="logout">
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>
        </div>
        <div id="main-content">
            <section class="content-header">
                @if (Auth::guest())
                    <h3>Please login</h3>
                @else
                    <h3>{{ Auth::user()->name }}</h3>
                    <h4>Web Developer</h4>
                @endif
            </section>
            <section class="content-container">
                @yield('content')
            </section>
        </div>
    </div>

    <!-- Scripts -->
    @include ('footer')
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
