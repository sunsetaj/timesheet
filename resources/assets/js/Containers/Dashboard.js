import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import format from 'date-fns/format';

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timesheets: WO.timesheets
        };
    }

    render() {
        let totalCount = this.state.timesheets.length;
        return (
            <div className="timesheets">
                <div className="timesheet-items">
                    <div className="timesheet-column primary-column">
                        <div className="header">
                            <h3>All Entries</h3>
                        </div>
                        <div className="entries">
                            {
                                totalCount > 0 ? (
                                    this.state.timesheets.map((entry, index) => {
                                        return (
                                            <div key={entry.id} className="single-entry">
                                                <div className="content">
                                                    <h4>{entry.task}<small>{entry.time_spent}HR</small><small>{format(entry.date, 'DD.MM.YYYY')}</small></h4>
                                                    <p>{entry.description}</p>
                                                </div>
                                            </div>
                                        )
                                    })
                                ) : (
                                <h4 className="empty">No timesheets found...</h4>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;
