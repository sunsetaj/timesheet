import React, { Component } from 'react';

import isSameDay from 'date-fns/is_same_day';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
// import getDay from 'date-fns/get_day';
// import startOfDay from 'date-fns/start_of_day';

class AllTimesheets extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            timesheets: WO.timesheets
        };
    }

    renderSingle(entry)
    {
        let tags = entry.tags;
        
        if (tags !== null && tags !== '') {
            tags = tags.split(',');
        } else {
            tags = [];
        }

        return (
            <div className="content">
                <h4>{entry.task}<small>{entry.time_spent}HR</small></h4>
                <p>{entry.description}</p>
                { 
                    tags.length > 0 ? (
                        <ul className="tags">
                            {
                                tags.map((tag, index) => {
                                    return ( <li key={index}>{tag}</li> )
                                })
                            }
                        </ul>
                    ) : (
                        ''
                    )
                }
                <span className="entry-option glyphicon glyphicon-option-vertical" aria-hidden="true">
                    <ul>
                        <li><a href="#">View/Edit</a></li>
                        <li><a href="#">Delete</a></li>
                    </ul>
                </span>
            </div>
        );
    }

    structureTimesheets(timesheets)
    {
        let lastDate = '';
        let returnObj = [];

        timesheets.map((entry, index) => {

            // console.log(index);
            // console.log(lastDate + ' : ' + entry.date);

            let date = entry.date;
            
            let tags = entry.tags;
            if (tags !== null && tags !== '') {
                tags = tags.split(',');
            } else {
                tags = [];
            }

            // let startOfDate = startOfDay(date);

            if (index === 0) {
                returnObj.push(
                    <div key={entry.id} className="main-entry">
                        <div className="header">
                            <h3>{ format(date, 'dddd') }</h3>
                            <span>{ format(date, 'DD.MM.YYYY') }</span>
                        </div>
                        <div className="entry sub-entry single-entry">
                            { this.renderSingle(entry) }
                        </div>
                    </div>
                );
                lastDate = entry.date;
            }
            else {
                returnObj.push( 
                    isSameDay(parse(entry.date), parse(lastDate)) ? (
                        // SAME DAY
                        <div key={entry.id} className="entry sub-entry single-entry">
                            { this.renderSingle(entry) }
                        </div>
                    ) : (
                        // DIFFERENT DAY
                        <div key={entry.id} className="main-entry">
                            <div className="header">
                                <h3>{ format(date, 'dddd') }</h3>
                                <span>{ format(date, 'DD.MM.YYYY') }</span>
                            </div>
                            <div className="entry sub-entry single-entry">
                                { this.renderSingle(entry) }
                            </div>
                        </div>
                    )
                );
                lastDate = entry.date;
            }

        });

        // console.log(returnObj);
        return returnObj;
    }

    render() {
        return(
            <div className="timesheet-items all-timesheets">
                <div className="timesheet-column primary-column">
                    <div className="entries">
                        { this.structureTimesheets(this.state.timesheets) }
                    </div>
                </div>
            </div>
        );
    }
}

export default AllTimesheets;
