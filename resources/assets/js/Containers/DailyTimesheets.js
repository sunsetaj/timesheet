import React, { Component } from 'react';

import isToday from 'date-fns/is_today';
import isYesterday from 'date-fns/is_yesterday';

import axios from 'axios';

import TimesheetList from '../Components/DailyTimesheets/TimesheetList';
import TimesheetCreate from '../Components/DailyTimesheets/TimesheetCreate';
import TimesheetModal from '../Components/DailyTimesheets/TimesheetModal';

class DailyTimesheets extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            timesheets: WO.timesheets,
            userId: WO.user,
            todaySheets: '',
            yesterdaySheets: '',
            showList: true,
            showModal: false,
            modalContent: []
        };
        this.showModal = this.showModal.bind(this);
    }

    componentWillMount() {
        let today = this.setDateCategory('today', this.state.timesheets);
        let yesterday = this.setDateCategory('yesterday', this.state.timesheets);
        this.setState({
            todaySheets: today,
            yesterdaySheets: yesterday
        });
    }

    setDateCategory(method, timesheets) {
        let sheets = [];
        switch(method) {
            case 'today':
                timesheets.map((item, index) => {
                    if (isToday(item.date)) {
                        sheets.push(item);
                    }
                });
                return sheets;
            case 'yesterday':
                timesheets.map((item, index) => {
                    if (isYesterday(item.date)) {
                        sheets.push(item);
                    }
                });
                return sheets;
        }
    }

    showModal(state, id) {
        let _this = this;
        console.log(state + ' ' + id);
        if (state == true) {
            axios({
                method: 'get',
                url: '/api/timesheet/' + id
            })
            .then(function(response) {
                let timesheet = response.data;
                _this.setState({
                    modalContent: timesheet,
                    showModal: state
                });
            })
            .catch(function(error) {
                console.log(error);
                alert(error);
            });
        } else {
            _this.setState({
                modalContent: [],
                showModal: state
            });
        }
    }

    // Fetch correct data from server and update state
    updateState() {
        let _this = this;
        axios({
            method: 'get',
            url: '/api/timesheets/daily/' + WO.user,
        })
        .then(function(response) {
            let timesheets = response.data;
            let todaySheets = _this.setDateCategory('today', timesheets);
            let yesterdaySheets = _this.setDateCategory('yesterday', timesheets);

            _this.setState({
                todaySheets: todaySheets,
                yesterdaySheets: yesterdaySheets,
                showList: true,
            });
        })
        .catch(function(error) {
            alert(error);
        });
    }

    // Determine which component to show
    showEdit() {
        this.setState({ showList: false });
    }

    showList() {
        this.setState({ showList: true });
    }

    render() {
        return(
            <div className="timesheets">
                <div className="sub-nav">
                    {this.state.showList == true ? (
                        <a className="btn btn-primary btn-sm" onClick={ this.showEdit.bind(this) }>Add Entry</a>
                    ): (
                        <a className="btn btn-danger btn-sm" onClick={ this.showList.bind(this) }>Cancel</a>
                    )}
                </div>
                <div>
                    { this.state.showList == true ? (
                        <TimesheetList
                            timesheets={this.state.timesheets}
                            daily={this.state.todaySheets}
                            yesterday={this.state.yesterdaySheets}
                            updateContainerState={this.updateState.bind(this)}
                            showModal={this.showModal}
                        />
                    ) : (
                        <TimesheetCreate
                            updateContainerState={this.updateState.bind(this)}
                        />
                    )}
                </div>
                { this.state.showModal == true ? (
                    <TimesheetModal
                        showModal={ this.showModal }
                        content={this.state.modalContent}
                        updateContainerState={this.updateState.bind(this)}
                    />
                ) : (
                    ''
                )}
            </div>
        );
    }
}

export default DailyTimesheets;
