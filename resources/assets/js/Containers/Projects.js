import React, { Component } from 'react';
import axios from 'axios';

import Errors from '../Components/Errors';

class Projects extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: WO.projects,
            showList: true,
            project_name: '',
            errors: '',
        };
    }

    resetFormState() {
        $('#create-project').removeAttr('disabled');
        $('#create-project').text('Create');
    }

    /**
     * Process creation request
     * @param {Form Event} evt
     */
    saveProject(evt) {
        evt.preventDefault();

        let formData = {
            project_name: this.state.project_name
        };

        let _this = this;
        $('#create-project').attr('disabled', 'disabled');
        $('#create-project').text('Creating...');

        axios({
            method: 'post',
            url: '/project',
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        .then(function(response) {
            $('#createProject')[0].reset();
            _this.setState({ 
                errors: '',
                project_name: '',
            });
            _this.resetFormState();
        })
        .catch(function(error) {
            let errors = error.response.data;
            _this.setState({ errors: errors });
            _this.resetFormState();
        });
    }

    addNewProject() {
        this.setState({ showList: false });
    }

    showProjectList() {
        this.setState({ showList: true });
    }

    /**
     * On Change event for description
     * @param {Event Object} evt
     */
    projectnameOnChange(evt) {
        this.setState({ project_name: evt.target.value });
    }

    render() {
        let projects = this.state.projects;
        let errors = this.state.errors;
        return (
            <div className="projects">
                { /* Navigation for Projects */}
                <div className="sub-nav">
                    { this.state.showList == true ? (
                        <a className="btn btn-primary btn-sm" onClick={this.addNewProject.bind(this)}>Add Project</a>
                    ) : (
                        <a className="btn btn-danger btn-sm" onClick={this.showProjectList.bind(this)}>Cancel</a>
                    )}
                </div>
                { /* Container for Project view */ }
                <div className="all-projects">
                    { projects.length > 0 ? (
                            <div className="primary-column project-column">
                                { /* List of all Projects */ }
                                <div className="header">
                                    <h3>All Projects</h3>
                                </div>
                                <div className="entries">
                                    { this.state.projects.map((project, index) => {
                                        return (
                                            <div key={project.id} className="single-project single-entry">
                                                <h4>{project.project_name}</h4>
                                            </div>
                                        )
                                    }) }
                                </div>
                            </div>
                    ) : (
                        <div className="primary-column project-column">
                            <div className="header">
                                <h3>No Projects Found</h3>
                            </div>
                        </div>
                    )}

                    { this.state.showList == true ? ( 
                        <div className="secondary-column project-column">
                            { /* Single Project information */ }
                            <div className="header">
                                <h3>Click on Project to view stats</h3>
                            </div>
                            <div className="entries empty">
                                <div className="single-entry">
                                    <h4>I wonder what this can be...</h4>
                                </div>
                            </div>
                        </div> 
                    ) : (
                        <div className="secondary-column project-column">
                            { /* Add new project Form */ }
                            <div className="header">
                                <h3>Add new Project</h3>
                            </div>
                            <div className="entries">
                                <form id="createProject" onSubmit={this.saveProject.bind(this)}>
                                    { errors ? <Errors errors={this.state.errors} /> : '' }
                                    <div className="form-group ">
                                        <label className="control-label requiredField">
                                            Project Name <span className="asteriskField">*</span>
                                        </label>
                                        <input required value={this.state.project_name} className="form-control" id="project_name" name="project_name" type="text" onChange={this.projectnameOnChange.bind(this)} />
                                    </div>
                                    <div className="form-group">
                                        <div>
                                            <button id="create-project" name="submit" type="submit">
                                                Create
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default Projects;