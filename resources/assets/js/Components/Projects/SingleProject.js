import React, { Component } from 'react';
import PropTypes from 'prop-types';

const SingleProject = (props) => {
    return (
        <h3>{props.project_name}</h3>
    );
}

SingleProject.propTypes = {
    project: PropTypes.array.isRequired
};

export default SingleProject;