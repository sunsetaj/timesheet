import React, { Component } from 'react';
import PropTypes from 'prop-types';

import startOfToday from 'date-fns/start_of_today';
import startOfYesterday from 'date-fns/start_of_yesterday';
import format from 'date-fns/format';

import axios from 'axios';

const TimesheetList = (props) => {

    let totalTime = 0;

    const removeEntry = (id) => {
        if (window.confirm('Are you sure?')) {
            axios({
                url: '/timesheet/delete/' + id,
                method: 'delete',
                data: { 'id': id },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .then(function(response) {
                props.updateContainerState();
            })
            .catch(function(error) {
                alert(error);
            });
        }
    }

    const renderListSection = (entries) => {
        let entryCount = entries.length;
        if (entryCount > 0) {
            return (
                entries.map((entry, index) => {
                    totalTime = totalTime + entry.time_spent;
                    let tags = entry.tags;
                    if (tags !== null && tags !== '') {
                        tags = tags.split(',');
                    } else {
                        tags = [];
                    }
                    return (
                        <div key={entry.id} className="single-entry">
                            <div className="content">
                                <h4>{entry.task}<small>{entry.time_spent}HR</small></h4>
                                <p>{entry.description}</p>
                                { 
                                    tags.length > 0 ? (
                                        <ul className="tags">
                                            {
                                                tags.map((tag, index) => {
                                                    return ( <li key={index}>{tag}</li> )
                                                })
                                            }
                                        </ul>
                                    ) : (
                                        ''
                                    )
                                }
                                <span className="entry-option glyphicon glyphicon-option-vertical" aria-hidden="true">
                                    <ul>
                                        <li><a onClick={() => {props.showModal(true, entry.id) }}>View/Edit</a></li>
                                        <li><a onClick={() => { removeEntry(entry.id) }}>Delete</a></li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                    );
                })
            );
        }
        else {
            return (
                <h4 className="empty">No timesheets found...</h4>
            );
        }
    };
    
    return (
        <div className="timesheet-items">
            <div className="timesheet-column primary-column">
                <div className="header">
                    <h3>Today</h3>
                    <span>{ format(startOfToday(), 'DD.MM.YYYY') }</span>
                </div>
                <div className="entries">
                    { renderListSection(props.daily) }
                </div>
                <p className="total">Total Time: {totalTime}hr</p>
            </div>
            <div className="timesheet-column secondary-column">
                <div className="header">
                    <h3>Yesterday</h3>
                    <span>{ format(startOfYesterday(), 'DD.MM.YYYY') }</span>
                </div>
                <div className="entries">
                    { renderListSection(props.yesterday) }
                </div>
            </div>
        </div>
    );
};

TimesheetList.propTypes = {
    timesheets: PropTypes.array.isRequired,
    daily: PropTypes.array.isRequired,
    yesterday: PropTypes.array.isRequired,
    updateContainerState: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired
};

export default TimesheetList;