import React, { Component } from 'react';
import PropTypes from 'prop-types';

import format from 'date-fns/format';
import axios from 'axios';

class TimesheetCreate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            task: '',
            description: '',
            time_spent: '',
            tags: '',
            date: format(Date.now(), 'YYYY-MM-DD'),
        };
    }

    /**
     * Process creation request
     * @param {Form Event} e
     */

    processRequest(e) {
        e.preventDefault();
        
        let formData = {
            task: this.state.task,
            description: this.state.description,
            time_spent: this.state.time_spent,
            tags: this.state.tags,
            date: this.state.date,
        };
        
        let _this = this;
        $('#create-submit').attr('disabled', 'disabled');
        $('#create-submit').text('Creating...');

        axios({
            method: 'post',
            url: '/timesheet',
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        .then(function(response){
            
            $('#createForm')[0].reset();
            $('#create-submit').removeAttr('disabled');
            $('#create-submit').text('Submit');
            _this.props.updateContainerState();
        })
        .catch(function(error) {
            alert(error);
        });

    }

    /**
     * On Change event for task text
     * @param {Event Object} evt
     */

    taskOnChange(evt) {
        this.setState({ task: evt.target.value });
    }

    /**
     * On Change event for description
     * @param {Event Object} evt
     */
    descriptionOnChange(evt) {
        this.setState({ description: evt.target.value });
    }
    
    /**
     * On Change event for time_spent
     * @param {Event Object} evt
     */
    timespentOnChange(evt) {
        this.setState({ time_spent: evt.target.value });
    }

    /**
     * On Change event for time_spent
     * @param {Event Object} evt
     */
    tagOnChange(evt) {
        this.setState({ tags: evt.target.value });
    }

    /**
     * On Change event for date
     * @param {Event Object} evt
     */
    dateOnChange(evt) {
        this.setState({ date: evt.target.value });
    }

    render() {
         return (
            <div className="timesheet-items">
                <div className="timesheet-column primary-column">
                    <div className="header">
                        <h3>Add New</h3>
                    </div>
                    <div className="entries">
                        <form id="createForm" onSubmit={this.processRequest.bind(this)}>
                            <div className="form-group ">
                                <label className="control-label requiredField">
                                    Task <span className="asteriskField">*</span>
                                </label>
                                <input required className="form-control" id="task" name="task" type="text" onChange={this.taskOnChange.bind(this)} />
                            </div>
                            <div className="form-group ">
                                <label className="control-label requiredField">
                                    Description <span className="asteriskField">*</span>
                                </label>
                                <textarea required className="form-control" cols="40" id="description" name="description" rows="2" onChange={this.descriptionOnChange.bind(this)}></textarea>
                            </div>
                            <div className="form-group ">
                                <label className="control-label requiredField">
                                    Time spent <span className="asteriskField">*</span>
                                </label>
                                <input required className="form-control" id="time_spent" name="time_spent" type="number" step="0.25" onChange={this.timespentOnChange.bind(this)} />
                            </div>
                            <div className="form-group">
                                <label className="control-label requiredField">
                                    Tags <span className="asteriskField"></span>
                                </label>
                                <input className="form-control" id="tags" name="tags" type="text" onChange={this.tagOnChange.bind(this)} />
                                <p className="input-helper">Seperate tags with a comma.</p>
                            </div>
                            <div className="form-group ">
                                <label className="control-label requiredField">
                                    Date <span className="asteriskField">*</span>
                                </label>
                                <input className="form-control" id="date" name="date" type="date" onChange={this.dateOnChange.bind(this)} />
                                <p className="input-helper">Date is set to Today by default.</p>
                            </div>
                            <div className="form-group">
                                <div>
                                    <button id="create-submit" name="submit" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

};

TimesheetCreate.propTypes = {
    updateContainerState: PropTypes.func.isRequired
};

export default TimesheetCreate;