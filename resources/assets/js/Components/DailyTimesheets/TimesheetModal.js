import React, { Component } from 'react';
import PropTypes from 'prop-types';

import format from 'date-fns/format';
import axios from 'axios';

class TimesheetModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            task: props.content.task,
            description: props.content.description,
            time_spent: props.content.time_spent,
            tags: props.content.tags,
            date: props.content.date,
        };
    }
    
    processUpdate(e) {
        e.preventDefault();

        let formData = {
            task: this.state.task,
            description: this.state.description,
            time_spent: this.state.time_spent,
            tags: this.state.tags,
            date: this.state.date,
        };

        let _this = this;
        let id = this.props.content.id;
        $('#update-submit').attr('disabled', 'disabled');
        $('#update-submit').text('Updating...');

        axios({
            method: 'put',
            url: '/timesheet/' + id,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        .then(function(response) {
            $('#editForm')[0].reset();
            _this.props.updateContainerState();
            _this.props.showModal(false, 0);
            $('#update-submit').removeAttr('disabled');
            $('#update-submit').text('Update');
        })
        .catch(function(error) {
            alert(error);
        });
    }

    /**
     * On Change event for task text
     * @param {Event Object} evt
     */

    taskOnChange(evt) {
        this.setState({ task: evt.target.value });
    }

    /**
     * On Change event for description
     * @param {Event Object} evt
     */
    descriptionOnChange(evt) {
        this.setState({ description: evt.target.value });
    }
    
    /**
     * On Change event for time_spent
     * @param {Event Object} evt
     */
    timespentOnChange(evt) {
        this.setState({ time_spent: evt.target.value });
    }

    /**
     * On Change event for time_spent
     * @param {Event Object} evt
     */
    tagOnChange(evt) {
        this.setState({ tags: evt.target.value });
    }

    /**
     * On Change event for date
     * @param {Event Object} evt
     */
    dateOnChange(evt) {
        this.setState({ date: evt.target.value });
    }

    render() {
        let content = this.props.content;
        let date = content.date;
        date = date.split(" ")[0];
        return (
            <div className="modal-popup">
                <div className="overlay" onClick={() => {this.props.showModal(false, 0)}}></div>
                <div className="entry-modal">
                    <form id="editForm" onSubmit={this.processUpdate.bind(this)}>
                        <div className="form-group ">
                            <label className="control-label requiredField">
                                Task <span className="asteriskField">*</span>
                            </label>
                            <input required className="form-control" onChange={this.taskOnChange.bind(this)} defaultValue={content.task} id="task" ref="task" name="task" type="text" />
                        </div>
                        <div className="form-group ">
                            <label className="control-label requiredField">
                                Description <span className="asteriskField">*</span>
                            </label>
                            <textarea required className="form-control" onChange={this.descriptionOnChange.bind(this)} defaultValue={content.description} cols="40" id="description" ref="description" name="description" rows="2"></textarea>
                        </div>
                        <div className="form-group ">
                            <label className="control-label requiredField">
                                Time spent <span className="asteriskField">*</span>
                            </label>
                            <input required className="form-control" onChange={this.timespentOnChange.bind(this)} defaultValue={content.time_spent} id="time_spent" ref="time_spent" name="time_spent" type="number" step="0.25" />
                        </div>
                        <div className="form-group">
                            <label className="control-label requiredField">
                                Tags <span className="asteriskField"></span>
                            </label>
                            <input className="form-control" id="tags" name="tags" type="text" defaultValue={content.tags} onChange={this.tagOnChange.bind(this)} />
                            <p className="input-helper">Seperate tags with a comma.</p>
                        </div>
                        <div className="form-group ">
                            <label className="control-label requiredField">
                                Date <span className="asteriskField">*</span>
                            </label>
                            <input required className="form-control" onChange={this.dateOnChange.bind(this)} defaultValue={date} id="date" ref="date" name="date" placeholder="MM/DD/YYYY" type="date" />
                        </div>
                        <div className="form-group">
                            <div>
                                <button id="update-submit" name="submit" type="submit">
                                    Update
                                </button>
                                <button className="btn btn-cancel" onClick={() => {this.props.showModal(false, 0)}}>Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

TimesheetModal.propTypes = {
    showModal: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    updateContainerState: PropTypes.func.isRequired
}

export default TimesheetModal;
