import React from 'react';
import PropTypes from 'prop-types';

const Errors = (props) => {
    let errors = props.errors;

    return (
        <div className="error-container">
            { Object.keys(errors).map((error, index) => {
                return (
                    <div key={index} className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        { errors[error] }
                    </div>
                )
            })}
        </div>
    );
};

Errors.propTypes = {
    errors: PropTypes.object.isRequired
};

export default Errors;