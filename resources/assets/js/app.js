require('./bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Dashboard from './Containers/Dashboard';
import AllTimesheets from './Containers/AllTimesheets';
import DailyTimesheets from './Containers/DailyTimesheets';
import Projects from './Containers/Projects';

ReactDOM.render((
    <Router>
        <div>
            <Route path="/home" component={Dashboard} />
            <Route exact path="/timesheet" component={AllTimesheets} />
            <Route path="/timesheet/daily" component={DailyTimesheets} />
            <Route exact path="/projects" component={Projects} />
        </div>
    </Router>
), document.getElementById('app-body'));
